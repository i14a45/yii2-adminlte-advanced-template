<?php


namespace backend\models\search;


use common\models\User;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class UserSearch
 * @package backend\models
 */
class UserSearch extends User
{
    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            [['id', 'status'], 'integer'],
            [['username', 'email'], 'string'],
            ['role', 'in', 'range' => array_keys(User::$roleList)],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find()
            ->select('u.*, aa.item_name as role')
            ->alias('u')
            ->joinWith(['authAssignment' => function($query) {
                /** @var ActiveQuery $query */
                $query->alias('aa');
            }])
            ->andWhere(['not in', 'status', User::STATUS_DELETED]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
                'attributes' => [
                    'id',
                    'username',
                    'email',
                    'created_at',
                    'updated_at',
                ],
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'aa.item_name' => $this->role,
        ]);
        $query->andFilterWhere(['like', 'username', $this->username]);
        $query->andFilterWhere(['like', 'email', $this->email]);

        return $dataProvider;
    }
}