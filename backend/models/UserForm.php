<?php


namespace backend\models;


use common\models\User;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class UserForm
 * @package backend\models
 */
class UserForm extends Model
{
    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var int
     */
    public $status;

    /** @var string */
    public $role;

    /**
     * @var User
     */
    private $_user;

    /**
     * {@inheritDoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => User::class, 'filter' => function($query) {
                if (!$this->getUser()->isNewRecord) {
                    $query->andWhere(['not', ['id' => $this->getUser()->id]]);
                }
            }],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => User::class, 'filter' => function($query) {
                if (!$this->getUser()->isNewRecord) {
                    $query->andWhere(['not', ['id' => $this->getUser()->id]]);
                }
            }],

            ['password', 'required', 'when' => function($model) {
                /** @var static $model */
                return $model->getUser()->isNewRecord;
            }, 'whenClient' => "function(attribute, value){
                return $('#user-id').val() == '';            
            }"],
            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],

            ['status', 'required'],
            ['status', 'integer'],
            ['status', 'in', 'range' => array_keys(User::$statusList)],

            ['role', 'in', 'range' => array_keys(User::$roleList)],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email' => 'E-mail',
            'password' => 'Пароль',
            'status' => 'Статус',
            'role' => 'Роль',
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function attributeHints()
    {
        return [
            'password' => 'Оставьте пустым для сохранения прежнего пароля',
        ];
    }

    /**
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->setAttributes($this->getAttributes(['username', 'email', 'status']), false);

            if ($user->isNewRecord) {
                $user->generateAuthKey();
                $user->generateEmailVerificationToken();
            }

            if ($this->password) {
                $user->setPassword($this->password);
            }

            if (!$user->save()) {
                throw new Exception('Error saving model');
            }

            $authManager = Yii::$app->authManager;
            $authManager->revokeAll($user->id);

            $roles = empty($this->role) ? [] : [$this->role];
            foreach ($roles as $role) {
                $authManager->assign($authManager->getRole($role), $user->id);
            }

            return true;
        }

        return false;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return !$this->_user ? new User() : $this->_user;
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    public function setUser(User $user)
    {
        $this->_user = $user;
        $this->setAttributes($user->getAttributes(['username', 'email', 'status']));
        $userRoles = Yii::$app->authManager->getRolesByUser($user->id);
        $this->role = array_key_first($userRoles);
    }
}