<?php

use yii\log\FileTarget;
use common\models\User;

$adminPath = '/admin';

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'container' => [
        'definitions' => [
            \backend\components\ActionColumn::class => [
                'buttonOptions' => [
                    'class' =>'btn btn-default btn-xs',
                ],
            ],
            \yii\grid\GridView::class => [
                'tableOptions' => [
                    'class' => 'table table-sm table-striped table-bordered'
                ],
                'layout' => "{items}\n{pager}",
                'pager' => [
                    'class' => \yii\bootstrap4\LinkPager::class,
                    'options' => [
                        'class' => 'pagination pagination-sm',
                    ],
                ],
            ],
            \yii\grid\DataColumn::class => [
                'filterInputOptions' => [
                    'class' => 'form-control form-control-sm',
                    'id' => null,
                ],
            ],
            \yii\bootstrap4\ActiveField::class => [
                'inputOptions' => ['class' => ['widget' => 'form-control form-control-sm']]
            ],
        ],
    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'csrfCookie' => [
                'httpOnly' => true,
                'path' => $adminPath,
            ],
        ],
        'user' => [
            'identityClass' => User::class,
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-backend',
                'path' => $adminPath,
                'httpOnly' => true
            ],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
            'cookieParams' => [
                'path' => $adminPath,
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => FileTarget::class,
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'formatter' => [
            'nullDisplay' => '',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'params' => $params,
];
