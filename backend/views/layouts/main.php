<?php

/* @var $this View */
/* @var $content string */

use backend\assets\AppAsset;
use common\models\User;
use i14a45\adminlte3\widgets\SidebarMenu;
use yii\bootstrap4\Breadcrumbs;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;
use yii\web\View;
use common\widgets\Alert;

/** @var User $user */
$user = Yii::$app->user->identity;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition sidebar-mini text-sm">
<?php $this->beginBody() ?>

<!-- Site wrapper -->
<div class="wrapper">
    <!-- Navbar -->
    <?php NavBar::begin([
        'options' => [
            'class' => 'main-header navbar navbar-expand navbar-white navbar-light',
        ],
        'collapseOptions' => [
            'tag' => false,
        ],
        'renderInnerContainer' => false,
    ]); ?>

    <!-- Left navbar links -->
    <?= Nav::widget([
        'items' => [
            [
                'label' => '<i class="fas fa-bars"></i>',
                'url' => '#',
                'linkOptons' => [
                    'data-widget' => 'pushmenu',
                    'role' => 'button',
                ],
                'encode' => false,
            ],
            ['label' => 'Home', 'url' => '#'],
            ['label' => 'Contact', 'url' => '#'],
        ],
        'options' => [
            'class' => 'navbar-nav',
        ],
    ]) ?>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                </button>
            </div>
        </div>
    </form>

    <!-- Right navbar links -->
    <?= Nav::widget([
        'items' => [
            [
                'label' => '<i class="far fa-comments"></i><span class="badge badge-danger navbar-badge">3</span>',
                'linkOptions' => [
                    'class' => [
                        'widget' => false,
                    ],
                ],
                'items' => [
                    [
                        'label' => '
                            <div class="media">
                                <img src="https://via.placeholder.com/128" alt="128x128" class="img-size-50 mr-3 img-circle">
                                <div class="media-body">
                                    <h3 class="dropdown-item-title">
                                        Brad Diesel
                                        <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                                    </h3>
                                    <p class="text-sm">Call me whenever you can...</p>
                                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                                </div>
                            </div>
                        ',
                        'url' => '#',
                    ],
                    '<div class="dropdown-divider"></div>',
                    [
                        'label' => 'See All Messages',
                        'url' => '#',
                        'linkOptions' => [
                            'class' => 'dropdown-footer',
                        ],
                    ],
                ],
                'dropdownOptions' => [
                    'class' => 'dropdown-menu dropdown-menu-lg dropdown-menu-right',
                ],
            ],
            [
                'label' => '<i class="far fa-bell"></i><span class="badge badge-warning navbar-badge">15</span>',
                'linkOptions' => [
                    'class' => [
                        'widget' => false,
                    ],
                ],
                'items' => [
                    '<span class="dropdown-item dropdown-header">15 Notifications</span>',
                    '<div class="dropdown-divider"></div>',
                    [
                        'label' => '
                            <i class="fas fa-envelope mr-2"></i> 4 new messages
                            <span class="float-right text-muted text-sm">3 mins</span>
                        ',
                        'url' => '#',
                    ],
                    '<div class="dropdown-divider"></div>',
                    [
                        'label' => 'See All Notifications',
                        'url' => '#',
                        'linkOptions' => [
                            'class' => 'dropdown-footer',
                        ],
                    ],
                ],
                'dropdownOptions' => [
                    'class' => 'dropdown-menu dropdown-menu-lg dropdown-menu-right',
                ],
            ],
            [
                'label' => '<i class="fas fa-user"></i> ' . $user->email,
                'items' => [
                    [
                        'label' => '...',
                        'url' => '#',
                    ],
                    '<div class="dropdown-divider"></div>',
                    [
                        'label' => 'Выход',
                        'url' => ['/site/logout'],
                        'linkOptions' => [
                            'data-method' => 'post'
                        ],
                    ],
                ],
            ],
        ],
        'options' => [
            'class' => 'navbar-nav ml-auto',
        ],
        'encodeLabels' => false,
    ]) ?>

    <?php NavBar::end(); ?>
    <!-- /.navbar -->



    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="<?= Url::home() ?>" class="brand-link">
            <img src="https://via.placeholder.com/128"
                 alt="AdminLTE Logo"
                 class="brand-image img-circle elevation-3"
                 style="opacity: .8">
            <span class="brand-text font-weight-light">AdminLTE 3</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="https://via.placeholder.com/160" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">Alexander Pierce</a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <?= SidebarMenu::widget([
                'items' => [
                    [
                        'label' => 'Menu item 1',
                        'url' => '#',
                        'icon' => 'newspaper',
                    ],
                    [
                        'label' => 'Menu item 2',
                        'url' => '#',
                        'icon' => 'video',
                    ],
                    [
                        'label' => 'Пользователи',
                        'url' => ['/user/index'],
                        'icon' => 'users',
                        'visible' => Yii::$app->user->can(User::ROLE_ADMIN),
                    ],
                    '<li class="nav-header">MULTI LEVEL EXAMPLE</li>',
                    [
                        'label' => 'Level 1',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Level 2',
                                'url' => '#',
                                'iconClassPrefix' => 'far fa-',
                            ],

                            [
                                'label' => 'Level 2',
                                'url' => '#',
                                'iconClassPrefix' => 'far fa-',
                                'items' => [
                                    [
                                        'label' => 'Level 3',
                                        'url' => '#',
                                        'iconClassPrefix' => 'far fa-',
                                        'icon' => 'dot-circle',
                                    ],
                                    [
                                        'label' => 'Level 3',
                                        'url' => '#',
                                        'iconClassPrefix' => 'far fa-',
                                        'icon' => 'dot-circle',
                                    ],
                                    [
                                        'label' => 'Level 3',
                                        'url' => '#',
                                        'iconClassPrefix' => 'far fa-',
                                        'icon' => 'dot-circle',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]) ?>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1><?= Html::encode($this->title) ?></h1>
                    </div>
                    <?php if (isset($this->params['breadcrumbs'])) { ?>
                    <div class="col-sm-6">
                        <?= Breadcrumbs::widget([
                            'links' => $this->params['breadcrumbs'],
                            'options' => [
                                'class' => 'float-sm-right',
                            ],
                        ]) ?>
                    </div>
                    <?php } ?>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <?= Alert::widget() ?>
            <?= $content ?>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        &copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?>
        <br>
        <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
    </footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
