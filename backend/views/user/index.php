<?php

use backend\models\search\UserSearch;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var UserSearch $searchModel */
/* @var ActiveDataProvider $dataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="card">
    <div class="card-header">
        <?= Html::a('<i class="fa fa-plus"></i> Добавить', ['create'], ['class' => 'btn btn-primary btn-sm']) ?>
    </div>
    <div class="card-body">
        <?= GridView::widget([
            'filterModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'columns' => [
                'id',
                'username',
                'email',
                [
                    'attribute' => 'role',
                    'filter' => User::$roleList,
                ],
                [
                    'attribute' => 'status',
                    'filter' => User::$statusList,
                    'value' => function($model) {
                        /** @var User $model */
                        return ArrayHelper::getValue(User::$statusList, $model->status);
                    }
                ],
                'created_at',
                'updated_at',
                [
                    'class' => backend\components\ActionColumn::class,
                    'template' => '{update} {delete}',
                    'visibleButtons' => [
                        'delete' => function($model) {
                            /** @var User $model */
                            return $model->id !== Yii::$app->user->id;
                        }
                    ],

                ],
            ],
        ]) ?>
    </div>
</div>
