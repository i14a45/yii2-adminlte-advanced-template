<?php

use backend\models\UserForm;
use common\models\User;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/* @var View $this */
/* @var UserForm $model */

$this->title = $model->getUser()->isNewRecord ? 'Новый пользователь' : 'Редкатирование пользователя #' . $model->getUser()->id;
$this->params['breadcrumbs'][] = [
    'label' => 'Пользователи',
    'url' => ['index'],
];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin() ?>
<div class="card">
    <div class="card-body">
        <?= $form->field($model, 'username')->textInput() ?>
        <?= $form->field($model, 'email')->textInput() ?>
        <?= $form->field($model, 'password')->textInput()
            ->hint($model->getUser()->isNewRecord ? false : $model->attributeHints()['password'])?>

        <?= $form->field($model, 'role')->dropDownList(User::$roleList, ['prompt' => '- Выбрать -']) ?>

        <?= $form->field($model, 'status')->dropDownList(User::$statusList, ['prompt' => '- Выбрать -']) ?>

        <?= Html::activeHiddenInput($model->getUser(), 'id') ?>
    </div>
    <div class="card-footer">
        <?= Html::submitButton('<i class="fa fa-check-circle"></i> Сохранить', ['class' => 'btn btn-sm btn-primary']) ?>
        <?= Html::a('<i class="fa fa-arrow-circle-left"></i> Вернуться', ['index'], ['class' => 'btn btn-sm btn-default']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>
