<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@www', dirname(dirname(__DIR__)) . '/web');

if (YII_DEBUG && !function_exists('dump')) {
    function dump($var, $die = true, $backtrace = 0)
    {
        echo '<pre>';
        var_dump($var);
        echo PHP_EOL;
        if ($backtrace > 0) {
            debug_print_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, $backtrace);
        }
        echo '</pre>';
        if ($die) {
            die();
        }
    }
}