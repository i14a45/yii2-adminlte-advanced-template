<?php

namespace console\controllers;

use common\models\User;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Users console controller
 */
class UserController extends Controller
{
    /**
     * Create action
     *
     * @return int
     */
    public function actionCreateAdmin($skip = 0)
    {
        $defaults = [
            $username = 'admin',
            $email = 'admin@example.com',
            $password = 'admin',
        ];

        if (!$skip) {
            $username = $this->prompt('Username', [
                'required' => true,
                'default' => $defaults['username'],
            ]);

            $email = $this->prompt('E-mail', [
                'required' => true,
                'default' => $defaults['email'],
            ]);

            $password = $this->prompt('Password', [
                'required' => true,
                'default' => $defaults['password'],
            ]);
        }

        $user = new User();
        $user->username = $username;
        $user->email = $email;
        $user->status = User::STATUS_ACTIVE;
        $user->setPassword($password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();

        if ($user->save()) {
            /** @var \yii\rbac\DbManager $authManager */
            $authManager = Yii::$app->authManager;
            $authManager->assign($authManager->getRole('admin'), $user->id);

            var_dump($user->attributes);
            return ExitCode::OK;
        }

        var_dump($user->errors);
        return ExitCode::UNSPECIFIED_ERROR;
    }
}