<?php


namespace frontend\models;


use Yii;

/**
 * Class LoginForm
 * @package frontend\models
 */
class LoginForm extends \common\models\LoginForm
{
    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
        }

        return false;
    }
}