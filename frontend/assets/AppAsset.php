<?php

namespace frontend\assets;

use i14a45\adminlte3\assets\AdminLteAsset;
use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
    ];
    public $depends = [
        AdminLteAsset::class,
    ];
}
